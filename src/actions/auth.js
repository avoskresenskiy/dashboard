import API from 'api';

export const AUTH_SUCCESS = 'AUTH_SUCCESS';

export function logIn() {
    return function(dispatch) {
        return API.login({
            login: "alytics.front.react@yandex.ru",
            password: "alyticstest1"
        }).then(response => {
            console.log("login success");
            dispatch(getToken());
        }).catch(error => {
            console.log("login error");
            alert("login error");
        });
    }
}

export function getToken() {
    return function(dispatch) {
        return API.getToken().then(response => {
            console.log("get token success", response);
            dispatch(authSuccess());
            // dispatch(getSources());
        }).catch(error => {
            console.log("get token error", error);
            alert("login error");
        });
    }
}

export function authSuccess() {
    return { type: AUTH_SUCCESS }
}
