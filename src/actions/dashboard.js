export const SELECT_INDICATOR = 'SELECT_INDICATOR';
export const SELECT_DATE_FROM = 'SELECT_DATE_FROM';
export const SELECT_DATE_TO = 'SELECT_DATE_TO';
export const SELECT_SOURCE = 'SELECT_SOURCE';

export const GET_SOURCES = 'GET_SOURCES';
export const GET_SOURCES_SUCCESS = 'GET_SOURCES_SUCCESS';
export const GET_SOURCES_FAIL = 'GET_SOURCES_FAIL';

export const GET_CHART_SOURCES = 'GET_CHART_SOURCES';
export const GET_CHART_SOURCES_SUCCESS = 'GET_CHART_SOURCES_SUCCESS';
export const GET_CHART_SOURCES_FAIL = 'GET_CHART_SOURCES_FAIL';

export const selectIndicator = payload => ({ type: SELECT_INDICATOR, payload });
export const selectDateFrom = payload => ({ type: SELECT_DATE_FROM, payload });
export const selectDateTo = payload => ({ type: SELECT_DATE_TO, payload });
export const selectSource = payload => ({ type: SELECT_SOURCE, payload });

export const getSources = () => ({ type: GET_SOURCES });
export const getSourcesSuccess = payload => ({ type: GET_SOURCES_SUCCESS, payload });
export const getSourcesFail = error => ({ type: GET_SOURCES_FAIL, error });

export const getChartSources = () => ({ type: GET_CHART_SOURCES });
export const getChartSourcesSuccess = payload => ({ type: GET_CHART_SOURCES_SUCCESS, payload });
export const getChartSourcesFail = error => ({ type: GET_CHART_SOURCES_FAIL, error });

// export function getSourcesThunk(params) {
//     return function(dispatch) {
//         return API.getSources(params).then(response => {
//             console.log("get sources success", response);
//         }).catch(error => {
//             console.log("get sources error", error);
//         });
//     }
// }