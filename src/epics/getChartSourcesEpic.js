import { GET_CHART_SOURCES, getChartSourcesSuccess, getChartSourcesFail } from 'actions/dashboard';
import API from 'api';

import moment from 'moment';
import { from, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';

const getChartSourcesEpic = (action$, state$) =>
    action$.pipe(
        ofType(GET_CHART_SOURCES),
        mergeMap(() => {
            const dashboard = state$.value.dashboard;
            const params = {
                project_id: 5600,
                offset: 0,
                limit: 9999999,
                format: 'json'
            };

            params.date_from = moment(dashboard.filter.dateFrom).format("YYYY-MM-DD");
            params.date_to = moment(dashboard.filter.dateTo).format("YYYY-MM-DD");

            let apiFunc = API.getChartSources;
            if(dashboard.filter.source) {
                params.utm_sourcemedium = dashboard.filter.source;
                apiFunc = API.getChartCompaings;
            }

            return from(apiFunc(params)).pipe(
                map(response => {
                    console.log("got chart response", response);
                    return getChartSourcesSuccess(response.data);
                }),
                catchError(error => of(getChartSourcesFail(error)))
            );
        })
    );

export default getChartSourcesEpic;