import { SELECT_SOURCE } from 'actions/dashboard';
import { getChartSources } from 'actions/dashboard'

import { mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { of } from 'rxjs';

const selectSourceEpic = action$ =>
    action$.pipe(
        ofType(SELECT_SOURCE),
        mergeMap(action => of(getChartSources()))
    );

export default selectSourceEpic;