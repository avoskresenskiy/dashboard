import { SELECT_DATE_FROM, SELECT_DATE_TO } from 'actions/dashboard';
import { getSources, getChartSources } from 'actions/dashboard'

import { mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { of } from 'rxjs';

const selectPeriodEpic = action$ =>
    action$.pipe(
        ofType(SELECT_DATE_FROM, SELECT_DATE_TO),
        mergeMap(action => of(
            getSources(),
            getChartSources()
        ))
    );

export default selectPeriodEpic;