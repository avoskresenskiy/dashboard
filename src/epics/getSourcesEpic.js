import { GET_SOURCES, getSourcesSuccess, getSourcesFail } from 'actions/dashboard';
import API from 'api';

import moment from 'moment';
import { from, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';

const getSourcesEpic = (action$, state$) =>
    action$.pipe(
        ofType(GET_SOURCES),
        mergeMap(() => {
            const dashboard = state$.value.dashboard;
            const params = {
                project_id: 5600,
                offset: 0,
                limit: 9999999,
                format: 'json'
            };

            params.date_from = moment(dashboard.filter.dateFrom).format("YYYY-MM-DD");
            params.date_to = moment(dashboard.filter.dateTo).format("YYYY-MM-DD");

            return from(API.getSources(params)).pipe(
                map(response => {
                    console.log("got response", response);
                    return getSourcesSuccess(response.data);
                }),
                catchError(error => of(getSourcesFail(error)))
            );
        })
    );

export default getSourcesEpic;