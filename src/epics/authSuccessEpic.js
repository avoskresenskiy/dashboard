import { AUTH_SUCCESS } from 'actions/auth';
import { getSources, getChartSources } from 'actions/dashboard'

import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';

const authSuccessEpic = action$ =>
    action$.pipe(
        ofType(AUTH_SUCCESS),
        mergeMap(action => of(
            getSources(),
            getChartSources()
        ))
    );

export default authSuccessEpic;