import React, { Component } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';
import 'react-datepicker/dist/react-datepicker.css';

registerLocale('ru', ru);

class PeriodSelector extends Component {
    render() {
        return (
            <div>
                <label>За период:</label>
                <DatePicker
                    onChange={this.selectDateFrom}
                    selected={this.props.dateFrom}
                    locale='ru'
                    dateFormat='dd.MM.yyyy'
                />
                &nbsp;&mdash;&nbsp;
                <DatePicker
                    onChange={this.selectDateTo}
                    selected={this.props.dateTo}
                    locale='ru'
                    dateFormat='dd.MM.yyyy'
                />
            </div>
        )
    }

    selectDateFrom = (date) => {
        this.props.selectDateFrom(date);
    }

    selectDateTo = (date) => {
        this.props.selectDateTo(date);
    }
};

export default PeriodSelector;

