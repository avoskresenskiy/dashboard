import React, { Component } from 'react';

class Clear extends Component {
    render() {
        return (
            <div className="g-clear" />
        );
    }
}

export default Clear;