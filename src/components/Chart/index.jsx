import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer } from 'recharts';
import moment from 'moment';

const CustomTooltip = ({ active, payload, label }) => {
    if (active) {
        return (
            <div className="custom-tooltip">
                <p className="label">{payload && payload[0] && payload[0].value}</p>
            </div>
        );
    }

    return null;
};

class Chart extends Component {
    render() {
        return (
            <div className="b-container__right">
                <h2>По дням</h2>
                <ResponsiveContainer width="100%" height={400}>
                    <LineChart
                        height={400}
                        data={this.props.data}
                    >
                        <XAxis dataKey="time" tickFormatter={timeStr => moment(timeStr).format('DD.MM.YYYY')} />
                        <YAxis dataKey="value" />
                        <Tooltip content={<CustomTooltip />} />
                        <CartesianGrid stroke="#f5f5f5" />
                        <Line dataKey="value" stroke="#55FF55" yAxisId={0} />
                    </LineChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default Chart;