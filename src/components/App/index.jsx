import React, { Component } from 'react';
import IndicatorSelector from 'containers/IndicatorSelector';
import PeriodSelector from 'containers/PeriodSelector';
import Source from 'containers/Source';
import Chart from 'containers/Chart';
import Clear from 'components/Common/Clear';

class App extends Component {
    render() {
        return (
            <div className="b-layout">
                <h1>Dashboard</h1>
                <div className="b-top-controls">
                    <div className="g-left">
                        <IndicatorSelector />
                    </div>
                    <div className="g-right">
                        <PeriodSelector/>
                    </div>
                    <Clear/>
                </div>
                <div className="b-container">
                    <Source/>
                    <Chart/>
                    <Clear/>
                </div>
                {this.props.children}
            </div>
        );
    }
}

export default App;
