import React, { Component } from 'react';
import RowTitle from './RowTitle';
import RowColumn from './RowColumn';
import Clear from 'components/Common/Clear';

class Total extends Component {
    render() {
        return (
            <div className="b-source b-source_total" onClick={this.handleClick}>
                <RowTitle>Все источники и в среднем</RowTitle>
                <RowColumn>{this.props.sum}</RowColumn>
                <RowColumn>{this.props.average}</RowColumn>
                <Clear/>
            </div>
        );
    }
    handleClick = () => {
        this.props.selectSource(null)
    }
}

export default Total;