import React, { Component } from 'react';

class RowTitle extends Component {
    render() {
        return (
            <div className="b-row__title">
                {this.props.children}
            </div>
        );
    }
}

export default RowTitle;