import React, { Component } from 'react';

class RowColumn extends Component {
    render() {
        return (
            <div className="b-row__column">
                {this.props.children}
            </div>
        );
    }
}

export default RowColumn;