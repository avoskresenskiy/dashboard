import React, { Component } from 'react';
import classnames from 'classnames';
import RowTitle from './RowTitle';
import RowColumn from './RowColumn';
import Diff from './Diff';
import Clear from 'components/Common/Clear';

class ListItem extends Component {
    render() {
        const classes = classnames({
            'b-source': true,
            'b-source_selected': this.props.selectedSource === this.props.name
        });
        return (
            <div className={classes} onClick={this.handleClick}>
                <RowTitle>{this.props.name}</RowTitle>
                <RowColumn>{this.props.value}</RowColumn>
                <RowColumn>
                    <Diff
                        value={(this.props.value - this.props.average).toFixed(2)}
                        percent={((this.props.value - this.props.average) / this.props.maxAbs * 100).toFixed(2)}
                    />
                </RowColumn>
                <Clear/>
            </div>
        );
    }

    handleClick = () => {
        this.props.selectSource(this.props.name)
    }
}

export default ListItem;