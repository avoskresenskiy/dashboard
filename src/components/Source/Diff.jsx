import React, { Component } from 'react';

class Diff extends Component {
    render() {
        return (
            <div className="b-diff">
                <div className="b-diff__value">{(this.props.value > 0 ? '+' : '') + this.props.value}</div>
                <div className="b-diff__progress">
                    {this.props.percent >= 0 &&
                        (<div className="b-diff__progress__bar b-diff__progress__bar_green" style={{width: (this.props.percent / 2) + '%'}}/>)
                    }
                    {this.props.percent < 0 &&
                        (<div className="b-diff__progress__bar b-diff__progress__bar_red" style={{left: Math.max((100 + parseFloat(this.props.percent)) / 2, 0) + '%'}}/>)
                    }
                </div>
            </div>
        );
    }
}

export default Diff;