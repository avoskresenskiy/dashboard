import React, { Component } from 'react';
import SourceTotal from 'containers/Source/Total';
import SourceList from 'containers/Source/List';

class Source extends Component {
    render() {
        return (
            <div className="b-container__left">
                <h2>По источникам</h2>
                <SourceTotal/>
                <SourceList/>
            </div>
        );
    }
}

export default Source;