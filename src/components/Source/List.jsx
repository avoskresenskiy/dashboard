import React, { Component } from 'react';
import ListItem from './ListItem';

class List extends Component {
    render() {
        return (
            <div>
                {this.props.objects.map(listItem => (
                    <ListItem
                        key={listItem.utm_sourcemedium}
                        name={listItem.utm_sourcemedium}
                        value={listItem.analytics[this.props.indicator]}
                        average={this.props.average}
                        maxAbs={this.props.maxAbs}
                        selectSource={this.props.selectSource}
                        selectedSource={this.props.selectedSource}
                    />
                ))}
            </div>
        );
    }
}

export default List;