import React, { Component } from 'react';

class Dropdown extends Component {
    render() {
        return (
            <select
                value={this.props.value}
                onChange={this.change}
            >
                {this.props.options && this.props.options.map((l, index) => (
                    <option key={index} value={l.value}>
                        {l.name}
                    </option>
                ))}
            </select>
        );
    }

    change = (e) => {
        this.props.onChange(e.target.value);
    }
}

export default Dropdown;