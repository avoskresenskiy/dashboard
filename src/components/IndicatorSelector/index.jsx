import React, { Component } from 'react';
import Dropdown from 'components/Dropdown';

class IndicatorSelector extends Component {
    render() {
        return (
            <div>
                <label>Оценить по показателю:</label>
                <Dropdown
                    options={this.props.indicators}
                    {...this.props}
                />
            </div>
        )
    }
};

export default IndicatorSelector;

