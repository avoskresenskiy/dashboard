import { connect } from 'react-redux';
import IndicatorSelector from 'components/IndicatorSelector';
import { selectIndicator } from 'actions/dashboard';

const mapStateToProps = (state) => {
    return {
        value: state.dashboard.filter.indicator,
        indicators: state.dashboard.indicators
    };
};

const mapDispatchToProps = {
    onChange: selectIndicator
};

export default connect(mapStateToProps, mapDispatchToProps)(IndicatorSelector);
