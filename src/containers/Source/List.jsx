import { connect } from 'react-redux';
import { selectSource } from 'actions/dashboard';
import SourceList from 'components/Source/List';

const mapStateToProps = (state) => {

    const getMax = (objects, indicator) => {
        return objects
            .filter(item => item.utm_sourcemedium !== "")
            .reduce((max, current) => current.analytics[indicator] > max ? current.analytics[indicator] : max, 0)
    };

    const getMin = (objects, indicator) => {
        return objects
            .filter(item => item.utm_sourcemedium !== "")
            .reduce((min, current) => current.analytics[indicator] < min ? current.analytics[indicator] : min, 0)
    };

    const maxAbs = (objects, indicator) => {
        const maxAbs = Math.abs(getMax(objects, indicator));
        const minAbs = Math.abs(getMin(objects, indicator));
        return Math.max(maxAbs, minAbs);
    }

    return {
        objects: (state.dashboard.sources.objects && state.dashboard.sources.objects.filter(item => item.utm_sourcemedium !== "").slice(0, 10)) || [],
        indicator: state.dashboard.filter.indicator,
        average: state.dashboard.sources.total && state.dashboard.sources.total.analytics[state.dashboard.filter.indicator].toFixed(2),
        maxAbs: (state.dashboard.sources.objects && maxAbs(state.dashboard.sources.objects, state.dashboard.filter.indicator)) || 0,
        selectedSource: state.dashboard.filter.source
    };
};

const mapDispatchToProps = {
    selectSource
};

export default connect(mapStateToProps, mapDispatchToProps)(SourceList);