import { connect } from 'react-redux';
import { selectSource } from 'actions/dashboard';
import SourceTotal from 'components/Source/Total';

const getSum = (objects, indicator) => {
    return objects
        .filter(item => item.utm_sourcemedium !== "")
        .reduce((sum, current) => sum + current.analytics[indicator], 0).toFixed(2);
};

const mapStateToProps = (state) => {
    return {
        average: state.dashboard.sources.total && state.dashboard.sources.total.analytics[state.dashboard.filter.indicator].toFixed(2),
        sum: state.dashboard.sources.objects && getSum(state.dashboard.sources.objects, state.dashboard.filter.indicator)
    };
};

const mapDispatchToProps = {
    selectSource
};

export default connect(mapStateToProps, mapDispatchToProps)(SourceTotal);