import { connect } from 'react-redux';
import Chart from 'components/Chart';

const mapStateToProps = (state) => {
    return {
        data: state.dashboard.chartData.analytics &&
            state.dashboard.chartData.analytics[state.dashboard.filter.indicator]
                .map(item => { return { time: item[0], value: item[1]} })
    };
};

export default connect(mapStateToProps)(Chart);