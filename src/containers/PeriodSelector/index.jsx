import { connect } from 'react-redux';
import PeriodSelector from 'components/PeriodSelector';
import { selectDateFrom, selectDateTo } from 'actions/dashboard';

const mapStateToProps = (state) => {
    return {
        dateFrom: state.dashboard.filter.dateFrom,
        dateTo: state.dashboard.filter.dateTo
    };
};

const mapDispatchToProps = {
    selectDateFrom,
    selectDateTo
};

export default connect(mapStateToProps, mapDispatchToProps)(PeriodSelector);
