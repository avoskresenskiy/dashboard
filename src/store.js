import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import reduxThunk from 'redux-thunk';
import authReducer from 'reducers/auth';
import dashboardReducer from 'reducers/dashboard';

import authSuccessEpic from 'epics/authSuccessEpic';
import selectPeriodEpic from 'epics/selectPeriodEpic';
import selectSourceEpic from 'epics/selectSourceEpic';
import getSourcesEpic from 'epics/getSourcesEpic';
import getChartSourcesEpic from 'epics/getChartSourcesEpic';

import moment from 'moment';

const initialState = {
    auth: {
        loggedIn: false
    },
    dashboard: {
        indicators: [
            {value: 'average_time', name: 'Время'},
            {value: 'bounce_rate', name: 'Отказы'},
            {value: 'sessions', name: 'Сессии'},
        ],
        filter: {
            dateFrom: moment().subtract(7, 'days').toDate(),
            dateTo: moment().toDate(),
            indicator: localStorage.getItem('indicator') || "average_time",
            project_id: 5600,
            offset: 0,
            limit: 9999999,
            source: null
        },
        sources: {},
        chartData: {}
    }
};

const rootReducer = combineReducers({
    auth: authReducer,
    dashboard: dashboardReducer
});

const rootEpic = combineEpics(
    authSuccessEpic,
    selectPeriodEpic,
    selectSourceEpic,
    getSourcesEpic,
    getChartSourcesEpic
);

const epicMiddleware = createEpicMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(
        applyMiddleware(reduxThunk, epicMiddleware)
    )
);

epicMiddleware.run(rootEpic);

store.subscribe(() => {
    const state = store.getState();
    if(state.dashboard && state.dashboard.filter && state.dashboard.filter.indicator) {
        localStorage.setItem('indicator', state.dashboard.filter.indicator)
    }
});

export default store;