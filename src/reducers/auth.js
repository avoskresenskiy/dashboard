import { AUTH_SUCCESS } from 'actions/auth'

const authReducer = (state = {}, action) => {
    switch (action.type) {
        case AUTH_SUCCESS:
            return {
                ...state,
                loggedIn: true
            };
        default:
            return state;
    }
}

export default authReducer;