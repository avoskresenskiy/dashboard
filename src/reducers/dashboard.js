import { SELECT_INDICATOR, SELECT_DATE_FROM, SELECT_DATE_TO, SELECT_SOURCE, GET_SOURCES_SUCCESS, GET_SOURCES_FAIL, GET_CHART_SOURCES_SUCCESS, GET_CHART_SOURCES_FAIL } from 'actions/dashboard'

const dashboardReducer = (state = {}, action) => {
    switch (action.type) {
        case SELECT_INDICATOR:
            return {
                ...state,
                filter: {
                    ...state.filter,
                    indicator: action.payload
                }
            };

        case SELECT_DATE_FROM:
            return {
                ...state,
                filter: {
                    ...state.filter,
                    dateFrom: action.payload
                }
            };

        case SELECT_DATE_TO:
            return {
                ...state,
                filter: {
                    ...state.filter,
                    dateTo: action.payload
                }
            };

        case SELECT_SOURCE:
            return {
                ...state,
                filter: {
                    ...state.filter,
                    source: action.payload
                }
            };

        case GET_SOURCES_SUCCESS:
            return {
                ...state,
                sources: action.payload
            };

        case GET_SOURCES_FAIL:
            alert('failed getting sources');
            console.log(action.error);
            return {
                ...state,
                sources: {}
            };

        case GET_CHART_SOURCES_SUCCESS:
            return {
                ...state,
                chartData: action.payload
            };

        case GET_CHART_SOURCES_FAIL:
            alert('failed getting chart sources');
            console.log(action.error);
            return {
                ...state,
                chartData: {}
            };

        default:
            return state;
    }
};

export default dashboardReducer;