import fetch from 'cross-fetch';
import axios from 'axios';
import qs from 'qs';

// const domain = 'testing.alytics.ru';
const domain = 'localhost';

class API {
    static login(credentials) {
        return fetch('http://' + domain + '/account/do-login',
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                credentials: 'include',
                method: "POST",
                body: qs.stringify(credentials)
            }
        );
        // c axios не проходит авторизация, статус 302 обрабатывает как ошибку. fetch - работает
        // не отлавливаем неправильный логин и пароль, т.к. статус одинаковый, а ответ в location: http://testing.alytics.ru/?wrong_password=true
        // а location подменяет локальный прокси-сервер для того, чтобы не было ошибок CORS
        /*return axios({
            method: 'post',
            url: 'http://' + domain + '/account/do-login',
            withCredentials: true,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: qs.stringify(credentials)
        });*/
    }

    static getToken() {
        return axios({
            url: 'http://' + domain + '/account/auth/access_token/?_=' + new Date().getTime(),
            withCredentials: true
        });
    }


    static getSources(params) {
        return axios({
            url: 'http://' + domain + '/sources/stat/sources/?' + qs.stringify(params),
            withCredentials: true
        });
    }

    static getChartSources(params) {
        return axios({
            url: 'http://' + domain + '/sources/stat/chart/sources/?' + qs.stringify(params),
            withCredentials: true
        });
    }

    static getChartCompaings(params) {
        return axios({
            url: 'http://' + domain + '/sources/stat/chart/campaigns/?' + qs.stringify(params),
            withCredentials: true
        });
    }
}

export default API;